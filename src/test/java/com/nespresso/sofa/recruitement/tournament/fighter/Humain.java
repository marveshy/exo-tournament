package com.nespresso.sofa.recruitement.tournament.fighter;

public interface Humain {
	public void engage(Humain humain);
	public int hitPoints();

}
