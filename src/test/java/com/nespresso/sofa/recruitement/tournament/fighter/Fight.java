package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.HashMap;
import java.util.Map;

public class Fight {
	private static final int VIKING_HITS_POINT = 6;
	private static final int SWORDSMAN_HITS_POINT = 5;
	private int vikingStrenghtPoints = 120;
	private int swordsmanStrenghtPoints = 100;
	private Map<Humain, Integer> hits = new HashMap<Humain, Integer>();

	public Map<Humain, Integer> fightBetweenHumain(Humain firstFighter) {
		Humain secondFighter = null;

		while (vikingStrenghtPoints != 0 && vikingStrenghtPoints != 0) {
			if (VIKING_HITS_POINT > swordsmanStrenghtPoints) {
				vikingStrenghtPoints = vikingStrenghtPoints - SWORDSMAN_HITS_POINT;
				swordsmanStrenghtPoints = 0;
				
//				if (Comparator.comparateViking(firstFighter)) {
					hits.put(firstFighter, vikingStrenghtPoints);
					hits.put(secondFighter,swordsmanStrenghtPoints); 
				
//				}
				
//				if ( Comparator.comparateSwordsman(firstFighter)) { 
//					hits.put(firstFighter, swordsmanStrenghtPoints);
//					hits.put(secondFighter, vikingStrenghtPoints); 					
//					
//			}
				break;
			}

			if (Comparator.comparateViking(firstFighter)) {
				secondFighter = new Swordsman();
				swordsmanHitsViking();
			}

//			if (Comparator.comparateSwordsman(firstFighter)) {
//				vinkingHitsSwordsman();
//			}
		}

		return hits;
	}

//	public void vinkingHitsSwordsman() {
//		vikingStrenghtPoints = vikingStrenghtPoints - SWORDSMAN_HITS_POINT;
//		swordsmanStrenghtPoints = swordsmanStrenghtPoints - VIKING_HITS_POINT;
//	}

	public void swordsmanHitsViking() {

		vikingStrenghtPoints = vikingStrenghtPoints - SWORDSMAN_HITS_POINT;
		swordsmanStrenghtPoints = swordsmanStrenghtPoints - VIKING_HITS_POINT;
	}

}
