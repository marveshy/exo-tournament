package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.HashMap;
import java.util.Map;

public class Swordsman implements Humain {

	private Fight fight ; 
	private Humain humain ;
	private Map<Humain, Integer> hits;
	private Viking viking;



	@Override
	public void engage(Humain humain) {
		fight = new Fight();
		this.humain=humain;		
		 hits= fight.fightBetweenHumain(humain);
		

	}

	@Override
	public int hitPoints() {
		int bloodLevel= hits.get(humain);		
		return bloodLevel;
	}
	


}
